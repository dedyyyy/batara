class CreateDokters < ActiveRecord::Migration[5.1]
  def change
    create_table :dokters do |t|
      t.integer :id_dokter
      t.boolean :status
      t.string :nama_dokter
      t.boolean :jenis_kelamin
      t.string :nama_puskesmas
      t.string :nama_poli
      t.date :jadwal_dokter
      t.boolean :senin
      t.boolean :selasa
      t.boolean :rabu
      t.boolean :kamis
      t.boolean :jumat
      t.boolean :sabtu
      t.boolean :minggu

      t.timestamps
    end
  end
end
